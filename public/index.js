var animals = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety", "One hundred", "One thousand", "One million"];
var lowerCaseAnimals = [];
var length = animals.length;
var unselectedAnimals = ["One", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Eleven", "Twelve", "Thirteen", "Fourteen", "Fifteen", "Sixteen", "Seventeen", "Eighteen", "Nineteen", "Twenty", "Thirty", "Forty", "Fifty", "Sixty", "Seventy", "Eighty", "Ninety", "One hundred", "One thousand", "One million"];
var correctAnswersCounters;
var incorrectAnswersCounters;
var currentPosition = -1;
var speech = null;
var answered = false;

initLowerCaseAnimals();
initAnswersCountersArrays();
randomizeQuestion();

function initLowerCaseAnimals()
{
    for (let index = 0; index < length; index++)
    {
        lowerCaseAnimals.push(animals[index].toLowerCase());
    }
}

function initAnswersCountersArrays() {
    correctAnswersCounters = new Array(length).fill(0);
    incorrectAnswersCounters = new Array(length).fill(0);
}

function marcador() {
    for (let index = 0; index < length; index++) {
        console.log(animals[index] + " -> Aciertos " + correctAnswersCounters[index] + " Fallos " + incorrectAnswersCounters[index]);
    }
}

function randomizeQuestion() {
    var rand = Math.floor(Math.random() * unselectedAnimals.length);
    currentPosition = animals.indexOf(unselectedAnimals[rand]);
    unselectedAnimals.splice(rand, 1);
    loadQuestionImage();
    loadSpeech()
}

function loadQuestionImage() {
    const image = document.createElement("img");
    image.src = "img/" + animals[currentPosition].toLowerCase() + ".jpg";
    document.querySelector(".speaker-container").appendChild(image);
}

function loadSpeech() {
    speech = new SpeechSynthesisUtterance(animals[currentPosition]);
    speech.lang = 'en-UK';
    document.querySelector(".fa-volume-up").addEventListener("click", playSpeech);
}

function playSpeech() {
    if (!answered)
    window.speechSynthesis.speak(speech);
}

function answerClicked() {
    if (!answered) {
        answered = true;
        var answerText = document.querySelector(".answer-text");
        var answerBox = document.querySelector(".answer").value.toLowerCase();
        answerText.style.opacity = 1;
        var answerIndex = lowerCaseAnimals.indexOf(answerBox);
        console.log(answerIndex);
        if (answerIndex == currentPosition) {
            answerText.textContent = animals[answerIndex];
            answerText.style.color = 'green';
            correctAnswersCounters[answerIndex]++;
        } else {
            answerText.textContent = "Error";
            answerText.style.color = 'red';
            incorrectAnswersCounters[currentPosition]++;
        }
        var s = answerText.style,
            step = 25 / (1500 || 300);
        s.opacity = s.opacity || 1;
        (function fade() {
            (s.opacity -= step) < 0 ? s.textContent = "" : setTimeout(fade, 25);
        })();
        setTimeout(nextQuestion, 1000);
    }
}

function nextQuestion() {
    answered = false;
    removeQuestionImage();
    clearAnswer();
    randomizeQuestion();
    if (unselectedAnimals.length == 0)
        unselectedAnimals = animals.slice(0);

}

function clearAnswer()
{
    document.querySelector(".answer").value = "";
}

function removeQuestionImage() {
    var elem = document.querySelector(".speaker-container img");
    elem.parentNode.removeChild(elem);
}
